package truckmoove

import "github.com/go-pg/pg/orm"

//TransportType model
type TransportType struct {
	TransportTypeID   string
	TransportTypeName string
	objectSaved       bool
}

//LoadObject
func (transportType *TransportType) LoadObject(ID string) error {
	err := Db.Model(&transportType).Where("transport_type_id = ?", ID).Select()
	transportType.objectSaved = Check(err) && transportType.TransportTypeID == ID
	return err
}

//SaveObject
func (transportType *TransportType) SaveObject() error {
	var (
		err error
		res orm.Result
	)
	if transportType.objectSaved {
		res, err = Db.Model(transportType).Where("transport_type_id = ?", transportType.TransportTypeID).Update()
	} else {
		res, err = Db.Model(transportType).Insert()
	}
	transportType.objectSaved = Check(err) && res.RowsAffected() == 1
	return err
}

//TransportTypes type
type TransportTypes []*TransportType

//LoadAll
func (transportTypes *TransportTypes) LoadAll() bool {
	err := Db.Model(&transportTypes).Select()
	return Check(err)
}

//GetByIDName
func (transportTypes *TransportTypes) GetByIDName(transportTypeID string) *TransportType {
	//	if fuelSystem == nil {}
	for _, transportType := range *transportTypes {
		if transportType.TransportTypeID == transportTypeID || transportType.TransportTypeName == transportTypeID {
			return transportType
		}
	}
	return nil
}
