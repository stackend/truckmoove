package truckmoove

import (
	"github.com/dewski/spatial"
	"github.com/go-pg/pg/orm"
)

//Address model
type Address struct {
	//	TableName    struct{} `sql:"address"`
	AddressID    string
	AddressHuman string
	AddressGeo   spatial.Point
	objectSaved  bool
	//	addressGeo   postgis.PointS
	//longitude float64
	//latitude  float64
}

//LoadById загрузить по ID
func (address *Address) LoadByID(addressID string) bool {
	err := Db.Model(&address).Where("address_id = ?", addressID).Select()
	address.objectSaved = Check(err) && address.AddressID == addressID
	return address.objectSaved
}

//Save
func (address *Address) Save() bool {
	var (
		err error
		res orm.Result
	)
	if address.objectSaved {
		res, err = Db.Model(address).Where("address_id = ?", address.AddressID).Update()
	} else {
		res, err = Db.Model(address).OnConflict("(address_geo) DO NOTHING").Insert()
		//если вставить запись не удалось, запросим модель по адресу
		if Check(err) && res.RowsAffected() == 0 {
			err = Db.Model(address).Where("address_geo = ?", address.AddressGeo).Select()
		}
	}
	address.objectSaved = Check(err)
	return address.objectSaved
}

//Addresses type
type Addresses []*Address
