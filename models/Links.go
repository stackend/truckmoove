package truckmoove

import "time"

//FuelSystemFuelStationLink model
type FuelSystemFuelStationLink struct {
	FuelStationID string
	FuelSystemID  string
}

//Save to db
func (link *FuelSystemFuelStationLink) Save() bool {
	_, err := Db.Model(&link).
		Where("fuel_station_id = ?", link.FuelStationID).
		Where("fuel_system_id = ?", link.FuelSystemID).
		SelectOrInsert()
	return Check(err)
}

//TransportFuelCardLink model
type TransportFuelCardLink struct {
	TransportFuelCardID    string
	FuelCardID             string
	TransportID            string
	TransFuelCardStartedAt time.Time
	TransFuelCardEndedAt   time.Time
}

//Save to db
func (link *TransportFuelCardLink) Save() bool {
	_, err := Db.Model(&link).Where("transport_fuel_card_id = ?", link.TransportFuelCardID).SelectOrInsert()

	s
	return Check(err)
}
