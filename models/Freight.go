package truckmoove

//Freight model
type Freight struct {
	FreightID            string
	FreightDeclaredValue int
	FreightGrossWeight   int
	FreightPackageAmount int
}

//loadObject
func (freight *Freight) loadObject(ID string) error {
	err := Db.Model(&freight).Where("freight_id = ?", ID).Select()
	return err
}
