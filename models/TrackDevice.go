package truckmoove

import "github.com/go-pg/pg/orm"

//TrackDevice model
type TrackDevice struct {
	TrackDeviceID       string
	TrackDeviceModel    string
	TrackDeviceObjectID int
	objectSaved         bool
}

//LoadObject
func (trackDevice *TrackDevice) LoadObject(ID string) error {
	err := Db.Model(&trackDevice).Where("track_device_id = ?", ID).Select()
	trackDevice.objectSaved = Check(err) && trackDevice.TrackDeviceID == ID
	return err
}

//SaveObject
func (trackDevice *TrackDevice) SaveObject() error {
	var (
		err error
		res orm.Result
	)
	if trackDevice.objectSaved {
		res, err = Db.Model(trackDevice).Where("track_device_id = ?", trackDevice.TrackDeviceID).Update()
	} else {
		res, err = Db.Model(trackDevice).Insert()
	}
	trackDevice.objectSaved = Check(err) && res.RowsAffected() == 1
	return err
}

//TrackDevices type
type TrackDevices []*TrackDevice
