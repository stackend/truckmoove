package truckmoove

import "github.com/go-pg/pg/orm"

//FuelType model
type FuelType struct {
	FuelTypeID   string
	FuelTypeName string
	objectSaved  bool
}

//LoadByID
func (fuelType *FuelType) LoadByID(fuelTypeID string) bool {
	err := Db.Model(&fuelType).Where("fuel_type_id = ?", fuelTypeID).Select()
	fuelType.objectSaved = Check(err) && fuelType.FuelTypeID == fuelTypeID
	return fuelType.objectSaved
}

//LoadByName
func (fuelType *FuelType) LoadByName(fuelTypeName string) bool {
	err := Db.Model(&fuelType).Where("fuel_type_name = ?", fuelTypeName).Select()
	fuelType.objectSaved = Check(err) && fuelType.FuelTypeName == fuelTypeName
	return fuelType.objectSaved
}

//Save
func (fuelType *FuelType) Save() bool {
	var (
		err error
		res orm.Result
	)
	if fuelType.objectSaved {
		res, err = Db.Model(fuelType).Where("fuel_type_id = ?", fuelType.FuelTypeID).Update()
	} else {
		res, err = Db.Model(fuelType).Insert()
	}
	fuelType.objectSaved = Check(err) && res.RowsAffected() == 1
	return fuelType.objectSaved
}

//Saved
func (fuelType *FuelType) Saved() bool {
	return fuelType.objectSaved
}

//FuelTypes type
type FuelTypes []*FuelType

//LoadAll
func (fuelTypes *FuelTypes) LoadAll() bool {
	err := Db.Model(&fuelTypes).Select()
	return Check(err)
}

//GetByIDName
func (fuelTypes *FuelTypes) GetByIDName(FuelTypeID string) *FuelType {
	//	if fuelSystem == nil {}
	for _, fuelType := range *fuelTypes {
		if fuelType.FuelTypeID == FuelTypeID || fuelType.FuelTypeName == FuelTypeID {
			return fuelType
		}
	}
	return nil
}
