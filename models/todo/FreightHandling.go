package truckmoove

import "time"

//FreightHandling - Перемещение груза
type FreightHandling struct {
	FreightHandlingID         string
	OrderID                   string
	WarehouseID               string
	FreightID                 string
	FreightHandlingOrder      int
	FreightHandlingTa         time.Time
	FreightHandlingTaByDriver time.Time
	FreightHandlingTaByGeo    time.Time
	FreightHandlingTlByGeo    time.Time
	FreightHandlingTlByDriver time.Time
	FreightHandlingUnload     bool
	FreightHandlingEta        time.Time
}

//LoadObject
func (freightHandling *FreightHandling) LoadObject(ID string) error {
	err := Db.Model(&freightHandling).Where("freight_handling_id = ?", ID).Select()
	return err
}

//GetOrder
func (freightHandling *FreightHandling) GetOrder() (*Order, error) {
	order := Order{}
	err := order.LoadObject(freightHandling.OrderID)
	return &order, err
}

//GetWarehouse
func (freightHandling *FreightHandling) GetWarehouse() (*Warehouse, error) {
	warehouse := Warehouse{}
	err := warehouse.LoadObject(freightHandling.WarehouseID)
	return &warehouse, err
}

//GetFreight
func (freightHandling *FreightHandling) GetFreight() (*Freight, error) {
	freight := Freight{}
	err := freight.loadObject(freightHandling.WarehouseID)
	return &freight, err
}
