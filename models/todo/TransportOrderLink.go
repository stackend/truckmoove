package truckmoove

import "time"

//TransportOrderLink model
type TransportOrderLink struct {
	TransportOrderID        string
	TransportID             string
	OrderID                 string
	UserID                  string
	TransportOrderStartedAt time.Time
	//	TransportOrderEndedAt   time.Time
}

//LoadObject
func (transportOrderLink *TransportOrderLink) LoadObject(ID string) error {
	err := Db.Model(&transportOrderLink).Where("transport_order_id = ?", ID).Select()
	return err
}

//GetTransport
func (transportOrderLink *TransportOrderLink) GetTransport() (*Transport, error) {
	transport := Transport{}
	err := transport.LoadObject(transportOrderLink.TransportID)
	return &transport, err
}

//GetOrder
func (transportOrderLink *TransportOrderLink) GetOrder() (*Order, error) {
	order := Order{}
	err := order.LoadObject(transportOrderLink.OrderID)
	return &order, err
}

//GetUser
func (transportOrderLink *TransportOrderLink) GetUser() (*User, error) {
	user := User{}
	err := user.LoadObject(transportOrderLink.UserID)
	return &user, err
}
