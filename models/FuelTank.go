package truckmoove

import "github.com/go-pg/pg/orm"

//FuelTank model
type FuelTank struct {
	FuelTankID       string
	TransportID      string
	FuelTypeID       string
	FuelTankCapacity float64
	objectSaved      bool
	transport        *Transport
	fuelType         *FuelType
}

//LoadByID
func (fuelTank *FuelTank) LoadByID(fuelTankID string) bool {
	err := Db.Model(&fuelTank).Where("fuel_tank_id = ?", fuelTankID).Select()
	fuelTank.objectSaved = Check(err) && fuelTank.FuelTankID == fuelTankID
	return fuelTank.objectSaved
}

//Save
func (fuelTank *FuelTank) Save() bool {
	var (
		err error
		res orm.Result
	)
	if fuelTank.objectSaved {
		res, err = Db.Model(&fuelTank).Where("fuel_tank_id = ?", fuelTank.FuelTankID).Update()
	} else {
		res, err = Db.Model(&fuelTank).Insert()
	}
	fuelTank.objectSaved = Check(err) && res.RowsAffected() == 1
	return fuelTank.objectSaved
}

//GetTransport
func (fuelTank *FuelTank) GetTransport() *Transport {
	if fuelTank.transport == nil || (fuelTank.TransportID != fuelTank.transport.TransportID) {
		fuelTank.transport = &Transport{}
		if !fuelTank.transport.LoadByID(fuelTank.TransportID) {
			fuelTank.transport = nil
		}
	}
	return fuelTank.transport
}

//GetFuelType
func (fuelTank *FuelTank) GetFuelType() *FuelType {
	if fuelTank.fuelType == nil || (fuelTank.FuelTypeID != fuelTank.fuelType.FuelTypeID) {
		fuelTank.fuelType = &FuelType{}
		if !fuelTank.fuelType.LoadByID(fuelTank.FuelTypeID) {
			fuelTank.fuelType = nil
		}
	}
	return fuelTank.fuelType
}

//FuelTanks type
type FuelTanks []FuelTank
