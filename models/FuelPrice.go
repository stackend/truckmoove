package truckmoove

import (
	"time"

	"github.com/go-pg/pg/orm"
)

//FuelPrice model
type FuelPrice struct {
	FuelPriceID        string
	FuelTypeID         string
	FuelStationID      string
	FuelPriceAmount    float64
	FuelPriceCreatedAt time.Time `sql:"-"`
	objectSaved        bool
}

//LoadByID
func (fuelPrice *FuelPrice) LoadByID(fuelPriceID string) bool {
	err := Db.Model(&fuelPrice).Where("fuel_price_id = ?", fuelPriceID).Select()
	fuelPrice.objectSaved = Check(err) && fuelPrice.FuelPriceID == fuelPriceID
	return fuelPrice.objectSaved
}

//Save
func (fuelPrice *FuelPrice) Save() error {
	var (
		err error
		res orm.Result
	)
	if fuelPrice.objectSaved {
		res, err = Db.Model(&fuelPrice).Where("fuel_price_id = ?", fuelPrice.FuelPriceID).Update()
	} else {
		res, err = Db.Model(&fuelPrice).Insert()
	}
	fuelPrice.objectSaved = Check(err) && res.RowsAffected() == 1
	return err
}

//Saved
func (fuelPrice *FuelPrice) Saved() bool {
	return fuelPrice.objectSaved
}

//FuelPrices type
type FuelPrices []*FuelPrice
