package truckmoove

import (
	"fmt"

	"github.com/go-pg/pg/orm"
)

//User model
type User struct {
	UserID             string
	UserLogin          string
	UserFirstName      string `sql:"-"`
	UserLastName       string `sql:"-"`
	UserPatronymicName string `sql:"-"`
	UserPhone          string `sql:"-"`
	objectSaved        bool
}

//LoadByID загрузить по ID
func (user *User) LoadByID(userID string) bool {
	err := Db.Model(&user).Where("user_id = ?", userID).Select()
	user.objectSaved = Check(err) && user.UserID == userID
	return user.objectSaved
}

//Save
func (user *User) Save() bool {
	var (
		err error
		res orm.Result
	)
	if user.objectSaved {
		res, err = Db.Model(user).Where("user_id = ?", user.UserID).Update()
	} else {
		res, err = Db.Model(user).Insert()
	}
	user.objectSaved = Check(err) && res.RowsAffected() == 1
	return user.objectSaved
}

//FullName Полное ФИО пользователя
func (user *User) FullName() string {
	return fmt.Sprintf("%s %s %s", user.UserLastName, user.UserFirstName, user.UserPatronymicName)
}

//Users type
type Users []*User
