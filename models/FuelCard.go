package truckmoove

import (
	"time"

	"github.com/go-pg/pg/orm"
)

//FuelCard model
type FuelCard struct {
	FuelCardID           string
	FuelSystemID         string
	OwnerCompanyID       string
	UsingCompanyID       string
	FuelCardNumber       string
	FuelCardApiID        string
	FuelCardApiStatus    string
	FuelCardComment      string
	FuelCardDateExpired  time.Time
	FuelCardDateReleased time.Time
	FuelCardLastUsage    time.Time
	FuelCardCreatedAt    time.Time
	FuelCardUpdatedAt    time.Time
	objectSaved          bool
	fuelSystem           *FuelSystem
	company              *Company
}

//LoadByID
func (fuelCard *FuelCard) LoadByID(fuelCardID string) bool {
	err := Db.Model(&fuelCard).Where("fuel_card_id = ?", fuelCardID).Select()
	fuelCard.objectSaved = Check(err) && fuelCard.FuelCardID == fuelCardID
	return fuelCard.objectSaved
}

//LoadByNumber
func (fuelCard *FuelCard) LoadByNumber(fuelCardNumber string) bool {
	err := Db.Model(&fuelCard).Where("fuel_card_number = ?", fuelCardNumber).Select()
	fuelCard.objectSaved = Check(err) && fuelCard.FuelCardNumber == fuelCardNumber
	return fuelCard.objectSaved
}

//Save
func (fuelCard *FuelCard) Save() bool {
	var (
		err error
		res orm.Result
	)
	if fuelCard.objectSaved {
		res, err = Db.Model(&fuelCard).Where("fuel_card_id = ?", fuelCard.FuelCardID).Update()
	} else {
		res, err = Db.Model(&fuelCard).Insert()
	}
	fuelCard.objectSaved = Check(err) && res.RowsAffected() == 1
	return fuelCard.objectSaved
}

//Saved
func (fuelCard *FuelCard) Saved() bool {
	return fuelCard.objectSaved
}
