package truckmoove

import (
	"time"

	"github.com/go-pg/pg/orm"
)

//FuelStation model
type FuelStation struct {
	FuelStationID        string
	AddressID            string
	FuelStationName      string
	FuelStationApiID     string
	FuelStationCreatedAt time.Time
	FuelStationUpdatedAt time.Time
	objectSaved          bool
	address              *Address
	fuelSystems          *FuelSystems
}

//LoadByID
func (fuelStation *FuelStation) LoadByID(StationID string) bool {
	err := Db.Model(&fuelStation).Where("fuel_station_id = ?", StationID).Select()
	fuelStation.objectSaved = Check(err) && fuelStation.FuelStationID == StationID
	return fuelStation.objectSaved
}

//LoadByName
func (fuelStation *FuelStation) LoadByName(StationName string) bool {
	err := Db.Model(&fuelStation).Where("fuel_station_name = ?", StationName).Select()
	fuelStation.objectSaved = Check(err) && fuelStation.FuelStationName == StationName
	return fuelStation.objectSaved
}

//Save
func (fuelStation *FuelStation) Save() bool {
	var (
		err error
		res orm.Result
	)
	if fuelStation.objectSaved {
		res, err = Db.Model(fuelStation).Where("fuel_station_id = ?", fuelStation.FuelStationID).Update()
	} else {
		res, err = Db.Model(fuelStation).Insert()
	}
	fuelStation.objectSaved = Check(err) && res.RowsAffected() == 1
	return fuelStation.objectSaved
}

//GetAddress
func (fuelStation *FuelStation) GetAddress() *Address {
	if fuelStation.address == nil || (fuelStation.AddressID != fuelStation.address.AddressID) {
		fuelStation.address = &Address{}
		if !fuelStation.address.LoadByID(fuelStation.AddressID) {
			fuelStation.address = nil
		}
	}
	return fuelStation.address
}

//Saved
func (fuelStation *FuelStation) Saved() bool {
	return fuelStation.objectSaved
}

//FuelStations type
type FuelStations []*FuelStation

//GetByIDName надем станцию по ID либо по имени
func (fuelStations *FuelStations) GetByIDName(FuelStationID string) *FuelStation {
	//	if fuelSystem == nil {}
	for _, fuelStation := range *fuelStations {
		if fuelStation.FuelStationID == FuelStationID || fuelStation.FuelStationName == FuelStationID {
			return fuelStation
		}
	}
	return nil
}
