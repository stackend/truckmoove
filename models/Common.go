package truckmoove

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/Sirupsen/logrus"
	"github.com/go-pg/pg"
	uuid "github.com/satori/go.uuid"
)

//Config file
type Config struct {
	PgConnection pg.Options
	Groups       []string
}

//Global vars
var (
	Db      *pg.DB
	Log     = logrus.New()
	Conf    = Config{}
	LastErr error
)

//GetUUID as string
func GetUUID() string {
	id := uuid.NewV1()
	return fmt.Sprintf("%s", id)
}

//Check error
func Check(err error) bool {
	if err != nil {
		Log.Error(err)
		//		panic(err)
		//os.Exit(1)
		LastErr = err
		return false
	} else {
		return true
	}
}

//ClearTables with delete from ...
func ClearTables() {
	queries := []string{
		`delete from transport_person_links`,
		`delete from transport_fuel_card_links`,
		`delete from transport_track_device_links`,
		`delete from fuel_prices`,
		`delete from fuel_cards`,
		`delete from fuel_stations`,
		`delete from fuel_systems`,
		`delete from fuel_types`,
		`delete from transports`,
		`delete from transport_types`,
		`delete from address`,
		`delete from companies`,
		`delete from users`,
		`delete from track_devices`,
	}

	for _, q := range queries {
		_, err := Db.Exec(q)
		if err != nil {
			panic(err)
		}
	}
}

/*func createModels(Models []interface{}) error {
	for _, model := range Models {
		err := db.CreateTable(model, &orm.CreateTableOptions{
			Temp: false, //true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}*/

func init() {
	file, _ := os.Open("config.json")
	json.NewDecoder(file).Decode(&Conf)
	Db = pg.Connect(&Conf.PgConnection)
}
