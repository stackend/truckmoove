package truckmoove

import (
	"time"

	"github.com/go-pg/pg/orm"
)

//PayTransaction model
type PayTransaction struct {
	PayTransactionID string
	FuelStationID    string
	FuelCardID       string
	FuelTypeID       string
	ProcessedAt      time.Time
	Price            float64
	Quantity         float64 //F_QTY
	objectSaved      bool
}

//LoadByID
func (payTransaction *PayTransaction) LoadByID(payTransactionID string) bool {
	err := Db.Model(&payTransaction).Where("pay_transaction_id = ?", payTransactionID).Select()
	payTransaction.objectSaved = Check(err) && payTransaction.PayTransactionID == payTransactionID
	return payTransaction.objectSaved
}

//Save
func (payTransaction *PayTransaction) Save() bool {
	var (
		err error
		res orm.Result
	)
	if payTransaction.objectSaved {
		res, err = Db.Model(&payTransaction).Where("pay_transaction_id = ?", payTransaction.PayTransactionID).Update()
	} else {
		res, err = Db.Model(&payTransaction).Insert()
	}
	payTransaction.objectSaved = Check(err) && res.RowsAffected() == 1
	return payTransaction.objectSaved
}

//Saved
func (payTransaction *PayTransaction) Saved() bool {
	return payTransaction.objectSaved
}
