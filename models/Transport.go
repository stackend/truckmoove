package truckmoove

import "github.com/go-pg/pg/orm"

//Transport model
type Transport struct {
	TransportID             string
	TransportTypeID         string
	CompanyID               string
	TransportModel          string
	TransportConsumption    int
	TransportPlateNumber    string
	TransportPackageAmount  int
	TransportAxesNum        int
	TransportPersonCapacity int
	objectSaved             bool
	transportType           *TransportType
	company                 *Company
}

//LoadByID
func (transport *Transport) LoadByID(TransportID string) bool {
	err := Db.Model(&transport).Where("transport_id = ?", TransportID).Select()
	transport.objectSaved = Check(err) && transport.TransportID == TransportID
	return transport.objectSaved
}

//LoadByNumber
func (transport *Transport) LoadByNumber(TransportPlateNumber string) bool {
	err := Db.Model(&transport).Where("transport_plate_number = ?", TransportPlateNumber).Select()
	transport.objectSaved = Check(err) && transport.TransportPlateNumber == TransportPlateNumber
	return transport.objectSaved
}

//Save
func (transport *Transport) Save() bool {
	var (
		err error
		res orm.Result
	)
	if transport.objectSaved {
		res, err = Db.Model(transport).Where("transport_id = ?", transport.TransportID).Update()
	} else {
		res, err = Db.Model(transport).Insert()
	}
	transport.objectSaved = Check(err) && res.RowsAffected() == 1
	return transport.objectSaved
}

//GetCompany
func (transport *Transport) GetCompany() *Company {
	if transport.company == nil || (transport.CompanyID != transport.company.CompanyID) {
		transport.company = &Company{}
		if !transport.company.LoadByID(transport.CompanyID) {
			transport.company = nil
		}
	}
	return transport.company
}

//GetTanks
func (transport *Transport) GetTanks() (FuelTanks, error) {
	var fuelTanks FuelTanks
	err := Db.Model(&fuelTanks).Where("transport_id = ?", transport.TransportID).Select()
	return fuelTanks, err
}

//Saved
func (transport *Transport) Saved() bool {
	return transport.objectSaved
}

//Transports type
type Transports []*Transport

//GetByIDNumber надем транспорт по ID либо по номеру
func (transports *Transports) GetByIDNumber(ID string) *Transport {
	//	if fuelSystem == nil {}
	for _, transport := range *transports {
		if transport.TransportID == ID || transport.TransportPlateNumber == ID || transport.TransportModel == ID {
			return transport
		}
	}
	return nil
}
