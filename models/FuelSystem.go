package truckmoove

import "github.com/go-pg/pg/orm"

//FuelSystem model
type FuelSystem struct {
	FuelSystemID     string
	FuelSystemName   string
	juridicalAddress string
	bankINN          string
	phone            string
	objectSaved      bool
}

//LoadObject
func (fuelSystem *FuelSystem) LoadObject(ID string) bool {
	err := Db.Model(&fuelSystem).Where("fuel_system_id = ?", ID).Select()
	return Check(err)
}

//SaveObject
func (fuelSystem *FuelSystem) SaveObject() error {
	var (
		err error
		res orm.Result
	)
	if fuelSystem.objectSaved {
		res, err = Db.Model(&fuelSystem).Where("fuel_station_id = ?", fuelSystem.FuelSystemID).Update()
	} else {
		res, err = Db.Model(&fuelSystem).Insert()
	}
	fuelSystem.objectSaved = Check(err) && res.RowsAffected() == 1
	return err
}

//FuelSystems type
type FuelSystems []FuelSystem

//GetSystem надем транспорт по ID либо по номеру
func (fuelSystems *FuelSystems) GetSystem(ID string) *FuelSystem {
	//	if fuelSystem == nil {}
	for _, fuelSystem := range *fuelSystems {
		if fuelSystem.FuelSystemID == ID || fuelSystem.FuelSystemName == ID {
			return &fuelSystem
		}
	}
	return nil
}

//LoadByFuelStation найдем бренды, которые привязаны к данной АЗС
func (fuelSystems *FuelSystems) LoadByFuelStation(ID string) bool {
	fuelSystems = nil //&FuelSystems{}
	links := []FuelSystemFuelStationLink{}

	b := false
	err := Db.Model(&links).Where("fuel_station_id = ?", ID).Select()

	if Check(err) {
		for _, link := range links {
			fuelSystem := FuelSystem{}
			if fuelSystem.LoadObject(link.FuelSystemID) {
				*fuelSystems = append(*fuelSystems, fuelSystem)
				b = true
			}
		}
	}
	return b
}

//LoadAll
func (fuelSystems *FuelSystems) LoadAll() bool {
	err := Db.Model(&fuelSystems).Select()
	return Check(err)
}
