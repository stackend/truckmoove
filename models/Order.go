package truckmoove

import "time"

//Order model
type Order struct {
	OrderID         string
	CompanyID       string
	UserID          string
	OrderPrice      int
	OrderCreatedAt  time.Time
	OrderActualTime time.Time
	OrderETC        time.Time
	company         *Company
	user            *User
}

//LoadObject a
func (order *Order) LoadObject(ID string) error {
	err := Db.Model(&order).Where("order_id = ?", ID).Select()
	return err
}

//GetCompany
func (order *Order) GetCompany() *Company {
	if order.company == nil || (order.CompanyID != order.company.CompanyID) {
		order.company = &Company{}
		if !order.company.LoadByID(order.CompanyID) {
			order.company = nil
		}
	}
	return order.company
}

//GetUser
func (order *Order) GetUser() *User {
	if order.user == nil || (order.UserID != order.user.UserID) {
		order.user = &User{}
		if !order.user.LoadByID(order.UserID) {
			order.user = nil
		}
	}
	return order.user
}
