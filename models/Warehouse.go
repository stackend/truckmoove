package truckmoove

//Warehouse model
type Warehouse struct {
	WarehouseID   string
	CompanyID     string
	AddressID     string
	WarehouseName string
	objectSaved   bool
	company       *Company
	address       *Address
}

//LoadByID загрузить по ID
func (warehouse *Warehouse) LoadByID(warehouseID string) bool {
	err := Db.Model(&warehouse).Where("warehouse_id = ?", warehouseID).Select()
	warehouse.objectSaved = Check(err) && warehouse.WarehouseID == warehouseID
	return warehouse.objectSaved
}

//GetAddress возвращает Address склада
func (warehouse *Warehouse) GetAddress() *Address {
	if warehouse.address == nil || (warehouse.AddressID != warehouse.address.AddressID) {
		warehouse.address = &Address{}
		if !warehouse.address.LoadByID(warehouse.AddressID) {
			warehouse.address = nil
		}
	}
	return warehouse.address
}

//GetCompany возвращает Company склада
func (warehouse *Warehouse) GetCompany() *Company {
	if warehouse.company == nil || (warehouse.CompanyID != warehouse.company.CompanyID) {
		warehouse.company = &Company{}
		if !warehouse.company.LoadByID(warehouse.CompanyID) {
			warehouse.company = nil
		}
	}
	return warehouse.company
}
