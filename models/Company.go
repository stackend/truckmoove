package truckmoove

import "github.com/go-pg/pg/orm"

//Company model
type Company struct {
	CompanyID        string
	CompanyName      string
	juridicalAddress string
	bankINN          string
	phone            string
	objectSaved      bool
}

//LoadByID
func (company *Company) LoadByID(ID string) bool {
	err := Db.Model(&company).Where("company_id = ?", ID).Select()
	company.objectSaved = Check(err) && company.CompanyID == ID
	return company.objectSaved
}

//Save
func (company *Company) Save() bool {
	var (
		err error
		res orm.Result
	)
	if company.objectSaved {
		res, err = Db.Model(company).Where("company_id = ?", company.CompanyID).Update()
	} else {
		res, err = Db.Model(company).Insert()
	}
	company.objectSaved = Check(err) && res.RowsAffected() == 1
	return company.objectSaved
}
